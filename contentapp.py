#!/usr/bin/python3



import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """recoge la informacion que necesitamos de la peticion del navegador y se lo pasamos a process"""
        metodo = request.split(' ')[0]
        recurso=  request.split(' ')[1]
        cuerpo= request.split('=')[-1]
        return (metodo, recurso, cuerpo)
 
    def process(self, resourceName):
        """realiza todo lo que hay que hacer para que funcione lo que nos piden """

        metodo, recurso, cuerpo= resourceName
        httpCode = "200 OK"

        final_formulario= ': <input name= "content" type="text" /> <input type= "submit" value= "Submit"></form>'
        htmlBody = "<html><body>" +  '<form action="" method="POST">'
        if metodo == "GET": #se busca en el diccionario
            if recurso in self.content: #se encuentra en el direccionario y se lo damos
                htmlBody +=  'recurso encontrado, si desea puede actualizar el contenido de ' + recurso \
                +  final_formulario \
                +  "el recurso llamada " + recurso +  " contiene " + self.content[recurso] \


            else: #no se encuntra en el diccionario y le damos la opcion de agregarlo
                htmlBody += 'no se ha encontrado el recurso, si desea puede crear el recurso ' + recurso + ' agregando su contenido'\
                + final_formulario


        elif metodo == "POST" :#se añade en el diccionario o se actualiza en ambos casos se hace lo mismo
            self.content[recurso] = cuerpo
            htmlBody += 'Ahora el recurso ' + recurso + ' contiene ' + cuerpo + ', puede actualizar de nuevo usando el formulario '\
            'o cambiar a otro recurso cambiando la URL ' + \
            final_formulario
        else:
            httpCode= "404 Error page"

        htmlBody +=   "</body></html>"
        return (httpCode, htmlBody)




if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
